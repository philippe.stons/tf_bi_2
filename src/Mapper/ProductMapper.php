<?php

namespace App\Mapper;

use App\Entity\Product;
use App\Model\ProductDTO;

class ProductMapper
{
    public static function entityToDto(Product $entity)
    {
        $dto = new ProductDTO();

        $dto->id = $entity->getId();
        $dto->name = $entity->getName();
        $dto->price = $entity->getPrice();
        $dto->category = $entity->getCategory();

        return $dto;
    }
}