<?php

namespace App\Mapper;

use App\Entity\User;
use UserDTO;

class UserMapper
{
    public static function entityToDto(User $entity)
    {
        $dto = new UserDTO();

        $dto->id = $entity->getId();
        $dto->username = $entity->getUserIdentifier();
        $dto->roles = $entity->getRoles();

        return $dto;
    }
}