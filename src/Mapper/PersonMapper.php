<?php

namespace App\Mapper;

use App\Entity\Person;
use App\Model\PersonDTO;

class PersonMapper
{
    public static function entityToDto(Person $entity)
    {
        $dto = new PersonDTO();

        $dto->id = $entity->getId();
        $dto->lastname = $entity->getLastname();
        $dto->firstname = $entity->getFirstname();
        $dto->admin = $entity->getAdmin();
        $dto->products = $entity->getProducts()->map(function($value) 
        {
            return ProductMapper::entityToDto($value);
        });
        
        return $dto;
    }
}