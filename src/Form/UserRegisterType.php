<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

class UserRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class,
        [
            'constraints' => [
                new NotBlank(), new Length(['max' => 30]) //, new Email()
            ]
        ]);

        $builder->add('password', TextType::class,
        [
            'constraints' => [
                new NotBlank(), new Length(['min' => 5]) //, new Regex()
            ]
        ]);
    }
}