<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->add('name', TextType::class,
        [
            'constraints' => [
                new NotBlank(), new Length(['max' => 255])
            ]
        ]);

        $builder->add('category', TextType::class,
        [
            'constraints' => [
                new NotBlank(), new Length(['max' => 10])
            ]
        ]);

        $builder->add('price', IntegerType::class);
    }
}