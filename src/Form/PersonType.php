<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->add('lastname', TextType::class,
        [
            'constraints' => [
                new NotBlank(), new Length(['max' => 255])
            ]
        ]);

        $builder->add('firstname', TextType::class,
        [
            'constraints' => [
                new NotBlank(), new Length(['max' => 255])
            ]
        ]);
    }
}