<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Product;
use App\Entity\User;
use App\Form\PersonType;
use App\Form\UserRegisterType;
use App\Mapper\PersonMapper;
use App\Mapper\UserMapper;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractFOSRestController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return void
     * 
     * @Rest\Post(path="/")
     * @Rest\View(statusCode="201")
     */
    public function postRegisterAction(Request $request, EntityManagerInterface $em)
    {
        $user = new User();

        $form = $this->createForm(UserRegisterType::class, $user);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        if($form->isSubmitted() && $form->isValid())
        {
            $user->setPassword($this->encoder->encodePassword($user, $user->getPassword()));
            $em->persist($user);
            $em->flush();

            return UserMapper::entityToDto($user);
        }

        return $this->handleView($this->view($form->getErrors()));
    }
}