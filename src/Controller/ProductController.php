<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Mapper\ProductMapper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class ProductController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/", name="api_product_getAll")
     * @Rest\View()
     * @return ProductDTO[]
     */
    public function getAllProductAction(EntityManagerInterface $em)
    {
        /** @var ProductRepository */
        $repository = $em->getRepository(Product::class);
        return array_map([ProductMapper::class, 'entityToDto'], $repository->findAll());
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $productID
     * 
     * @Rest\Get(path="/{productID}", requirements={ "productID" : "\d+" })
     * @Rest\View()
     * @return ProductDTO[]
     */
    public function getOneProductByIdAction(Request $request, EntityManagerInterface $em, $productID)
    {
        /** @var Product $item */
        $item = $em->getRepository(Product::class)->find($productID);
        if($item != null)
        {
            return ProductMapper::entityToDto($item);
        }
        return null;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return ProductDTO|View|Symfony\Component\Form\FormInterface
     * 
     * @Rest\Post(path="/")
     * @Rest\View(statusCode="201")
     */
    public function postProductAction(Request $request, EntityManagerInterface $em)
    {
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        if($form->isSubmitted() && $form->isValid())
        {
            $em->persist($product);
            $em->flush();

            return ProductMapper::entityToDto($product);
            //return $this->handleView(
            //    $this->view(['status' => "ok"], Response::HTTP_CREATED)
            //);
        }

        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $productID
     * 
     * @Rest\Put(path="/{productID}", requirements={ "productID" : "\d+" })
     * @Rest\View()
     * @return ProductDTO[]
     */
    public function putGoalAction(Request $request, EntityManagerInterface $em, $productID)
    {
        $product = $em->getRepository(Product::class)->find($productID);

        if($product != null)
        {
            $form = $this->createForm(ProductType::class, $product);
            $data = json_decode($request->getContent(), true);
            $form->submit($data);

            if($form->isSubmitted() && $form->isValid())
            {
                $em->persist($product);
                $em->flush();

                return ProductMapper::entityToDto($product);
            }
            return $this->handleView($this->view($form->getErrors()));
        }
        return null;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $productID
     * 
     * @Rest\Delete(path="/{productID}", requirements={ "productID" : "\d+" })
     * @Rest\View()
     * @return ProductDTO[]
     * @throws
     */
    public function deleteOneAction(Request $request, EntityManagerInterface $em, $productID)
    {
        /** @var Product $item */
        $item = $em->getRepository(Product::class)->find($productID);
        if($item != null)
        {
            $em->remove($item);
            $em->flush();
            return $this->handleView(
               $this->view(['status' => "ok"], Response::HTTP_CREATED)
            );
        }
        throw new NotFoundHttpException("not found");
    }
}