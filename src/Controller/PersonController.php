<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Product;
use App\Form\PersonType;
use App\Mapper\PersonMapper;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class PersonController extends AbstractFOSRestController
{
    /**
     * @param EntityManagerInterface $em
     * 
     * @Rest\Get(path="/", name="api_person_getAll")
     * @Rest\View()
     * @return PersonDTO[]
     */
    public function getAllPersonAction(EntityManagerInterface $em)
    {
        $item = $em->getRepository(Person::class)->findAll();
        return array_map([PersonMapper::class, 'entityToDto'], $item);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $personID
     * 
     * @Rest\Get(path="/{personID}", name="api_person_getAll", requirements={ "personID" : "\d+" })
     * @Rest\View()
     * @return PersonDTO
     */
    public function getOnePersonByIdAction(Request $request, EntityManagerInterface $em, $personID)
    {
        $item = $em->getRepository(Person::class)->find($personID);
        if($item != null)
        {
            return PersonMapper::entityToDto($item);
        }
        return null;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return PersonDTO|View|Symfony\Component\Form\FormInterface
     * 
     * @Rest\Post(path="/")
     * @Rest\View(statusCode="201")
     */
    public function postPersonAction(Request $request, EntityManagerInterface $em)
    {
        $person = new Person();

        $form = $this->createForm(PersonType::class, $person);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);

        if($form->isSubmitted() && $form->isValid())
        {
            $em->persist($person);
            $em->flush();

            return PersonMapper::entityToDto($person);
        }
        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $personID
     * 
     * @Rest\Put(path="/{personID}", requirements={ "personID" : "\d+" })
     * @Rest\View()
     * @return PersonDTO
     */
    public function putPersonAction(Request $request, EntityManagerInterface $em, $personID)
    {
        $person = $em->getRepository(Person::class)->find($personID);

        if($person != null)
        {
            $form = $this->createForm(PersonType::class, $person);
            $data = json_decode($request->getContent(), true);

            $form->submit($data);

            if($form->isSubmitted() &&$form->isValid())
            {
                $em->persist($person);
                $em->flush();

                return PersonMapper::entityToDto($person);
            }
            return $this->handleView($this->view($form->getErrors()));
        }
        return null;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $personID
     * @param $productID
     * @return PersonDTO
     * 
     * @Rest\Put(path="/{personID}/add/{productID}", requirements={ "personID" : "\d+", "productID" : "\d+" })
     * @Rest\View()
     */
    public function putAddProductAction(Request $request, EntityManagerInterface $em, $personID, $productID)
    {
        /** @var Person $person */
        $person = $em->getRepository(Person::class)->find($personID);
        /** @var Product $product */
        $product = $em->getRepository(Product::class)->find($productID);

        if($person != null &&  $product != null)
        {
            $person->addProduct($product);
            $em->persist($person);
            $em->flush();
            return PersonMapper::entityToDto($person);
        }
        return null;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $personID
     * @param $productID
     * @return PersonDTO
     * 
     * @Rest\Put(path="/{personID}/remove/{productID}", requirements={ "personID" : "\d+", "productID" : "\d+" })
     * @Rest\View()
     */
    public function putRemoveProductAction(Request $request, EntityManagerInterface $em, $personID, $productID)
    {
        /** @var Person $person */
        $person = $em->getRepository(Person::class)->find($personID);
        /** @var Product $product */
        $product = $em->getRepository(Product::class)->find($productID);

        if($person != null &&  $product != null)
        {
            $person->removeProduct($product);
            $em->persist($person);
            $em->flush();
            return PersonMapper::entityToDto($person);
        }
        return null;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $personID
     * 
     * @Rest\Delete(path="/{personID}", requirements={ "personID" : "\d+" })
     * @Rest\View()
     * @return mixed
     */
    public function deletePersonAction(Request $request, EntityManagerInterface $em, $personID)
    {
        $person = $em->getRepository(Person::class)->find($personID);

        if($person != null)
        {
            $em->remove($person);
            $em->flush();

            return $this->handleView(
                $this->view(['status' => "ok"], Response::HTTP_CREATED)
            );
        }
        return null;
    }
}