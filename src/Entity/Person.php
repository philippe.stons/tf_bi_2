<?php 

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="PersonID", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @var Product[]
     * 
     * @ORM\ManyToMany(targetEntity="Product")
     * @ORM\JoinTable(
     *  name="PersonProduct",
     *  joinColumns=
     *  { 
     *      @ORM\JoinColumn(name="PersonID", referencedColumnName="PersonID") 
     *  },
     *  inverseJoinColumns= 
     *  {
     *      @ORM\JoinColumn(name="ProductID", referencedColumnName="ProductID")
     *  }
     * )
     */
    private $products;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="admin", type="boolean", nullable=false, options={ "default" : FALSE })
     */
    private $admin;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->admin = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

    public function getAdmin(): ?bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin): self
    {
        $this->admin = $admin;

        return $this;
    }
}