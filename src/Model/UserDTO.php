<?php

use JMS\Serializer\Annotation as Serializer;

class UserDTO
{
    /**
     * @var integer
     * 
     * @Serializer\SerializedName("id")
     */
    public $id;

    /**
     * @var string
     * 
     * @Serializer\SerializedName("username")
     */
    public $username;

    /**
     * @var string[]
     * 
     * @Serializer\SerializedName("roles")
     */
    public $roles;
}