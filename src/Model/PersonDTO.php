<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class PersonDTO
{
    /**
     * @var integer
     * 
     * @Serializer\SerializedName("id")
     */
    public $id;

    /**
     * @var string
     * 
     * @Serializer\SerializedName("firstname")
     */
    public $firstname;

    /**
     * @var string
     * 
     * @Serializer\SerializedName("lastname")
     */
    public $lastname;

    /**
     * @var ProductDTO[]
     * 
     * @Serializer\SerializedName("products")
     */
    public $products;

    /**
     * @var boolean
     * 
     * @Serializer\SerializedName("admin")
     */
    public $admin;
}