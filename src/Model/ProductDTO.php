<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;

class ProductDTO
{
    /**
     * @var integer
     * 
     * @Serializer\SerializedName("id")
     */
    public $id;

    /**
     * @var string
     * 
     * @Serializer\SerializedName("name")
     */
    public $name;

    /**
     * @var float
     * 
     * @Serializer\SerializedName("price")
     */
    public $price;

    /**
     * @var string
     * 
     * @Serializer\SerializedName("category")
     */
    public $category;
}